//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input(char parameters)
{
   float x;
   printf("Enter the Dimensions of Tromboloid for %c:\n", parameters);
   scanf("%f",&x);
   return x;
}
float compute_vol(float h,float d, float b)
{
   float vol;
   vol=((h*d)+d)/(b*3);
   return vol;
}
void output (float v,float h, float d, float b)
{
   printf("Volume of Tromboloid with dimensions %.2f,%.2f,%.2f is  %.2f /n",h,d,b,v);
}
int main()
{
   float h,d,b,v;
   h=input('h');
   d=input('d');
   b=input ('b');
   v=compute_vol(h,d,b);
   output(v,h,d,b);
   return 0;
}
